defmodule Stallion.IntegrationTest do
  use ExUnit.Case

  defmodule Signal do
    def post do
      receive do
        {:release} -> :ok
        _ -> :error
      after
        5_000 -> IO.puts "Timeout"
      end
    end
  end

  defmodule TestModule do
    use Stallion.Module
    alias Stallion.Command.Manager

    hear ~r/alles/ do
      send :test, {:called_back, input}
    end

    hear ~r/not_called/ do
      send :test, {:throw_error, "foo"}
    end

    hear ~r/slow/ do
      send :test, {:slow, input, args[:env]}
    end

    hear ~r/guard wait/ do
      task = Task.async fn() -> Signal.post end
      Process.register task.pid, :guard
    end

    hear ~r/guard release/ do
      x = send :guard, {:release}
      send :test, {:called_back, x}
    end

    hear ~r/match (?<digit>\d+)/ do
      send :test, {:match, captures["digit"], args[:env]}
    end

    exit_code 0, args do
      send :test, {:all_went_ok, args}
    end

    exit_code code, _args do
      send :test, {:not_ok, code}
    end

    get "/plain" do
      send_resp(conn, 201, Poison.encode!(%{ok: true}))
    end

    get "/long" do
      run "sleep 10"
      send_resp(conn, 201, Poison.encode!(%{ok: true}))
    end

    get "/cmd" do
      {:ok, pid} = run "echo 'alles ok'", args: [:some_args]
      Manager.await(Manager, pid)
      send_resp(conn, 201, Poison.encode!(%{ok: true}))
    end

    get "/subsequent/:one/:two" do
      {:ok, pid} =
        run "echo \"slow\"; sleep 0.1; echo 'match #{one}\n'; sleep 0.1; echo 'match #{two}'",
            args: [env: "subsequent"]

      Manager.await(Manager, pid)
      send_resp(conn, 201, Poison.encode!(%{ok: true}))
    end

    post "/guard/:guard" do
      run "echo 'guard #{guard}'"
      send_resp(conn, 201, Poison.encode!(%{ok: true}))
    end
  end

  use Stallion.Web.ConnCase, async: true
  setup do
    Application.put_env(:stallion, :routes, %{"custom" => TestModule })

    Stallion.Module.Registry.register(Stallion.Module.Registry, TestModule)
    on_exit fn() ->
      Stallion.Module.Registry.unregister(Stallion.Module.Registry, TestModule)
    end

    Process.register self, :test

    # FIXME start Endpoint without explicit server: true in config.test.exs
    HTTPoison.start
    TestModule.start_link
    :ok
  end

  test "http requests work" do
    response = HTTPoison.get! "http://127.0.0.1:6666/custom/plain"
    assert Poison.decode!(response.body) == %{"ok" => true}
  end

  test "404 works" do
    response = HTTPoison.get! "http://127.0.0.1:6666/not_here"
    assert Poison.decode!(response.body) == %{"message" => "Page not found", "status" => 404}
  end

  test "http requests work with cmd inside in non-blocking way" do
    conn = get build_conn(), "/custom/cmd"
    response = json_response(conn, 201)
    assert response == %{"ok" => true}
    assert_receive {:called_back, "alles ok"}
    assert_receive {:all_went_ok, [:some_args]} # exit code assert
  end

  test "subsequent commands" do
    conn = get build_conn(), "/custom/subsequent/1/2"
    response = json_response(conn, 201)
    assert response == %{"ok" => true}

    assert_receive {:slow, "slow", "subsequent"}, 500
    assert_receive {:match, "1", "subsequent"}
    assert_receive {:match, "2", "subsequent"}
    assert_receive {:all_went_ok, _}
  end

end
