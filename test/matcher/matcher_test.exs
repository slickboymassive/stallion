defmodule MatcherTest do
  use ExUnit.Case, async: true
  alias Stallion.Command

  defmodule Stdout do
    use GenEvent

    def handle_event(%{type: :out, data: input, args: _args}, state) do
      Stallion.Matcher.match(:test, input)
      {:ok, state}
    end

    def handle_event(_, state) do
      # ignore rest
      {:ok, state}
    end

  end

  setup do
    Process.register self, :matcher_test
    {:ok, matcher} = Stallion.Matcher.start_link(name: :test)

    Stallion.Matcher.add_matcher(matcher, ~r/^2/, fn(input, _args, _captures) ->
      send :matcher_test, {:called_back, "input: #{input}"}
    end)

    {:ok, %{matcher: matcher}}
  end

  test "matchers work", %{matcher: matcher} do
    Enum.each(["2", "32", "2"], fn(item) ->
      Stallion.Matcher.match(matcher, item)
    end)

    assert_receive {:called_back, "input: 2"}
    assert_receive {:called_back, "input: 2"}
  end

  test "can listen to commands" do
    {:ok, gen_event_pid} = GenEvent.start_link
    GenEvent.add_handler(gen_event_pid, Stdout, [])
    {:ok, pid} = Command.Runner.start(gen_event_pid)

    Command.Runner.run(pid, "echo 2; sleep 0; echo 32; sleep 0; echo 2;", 10_000)
    assert_receive {:called_back, "input: 2"}
    assert_receive {:called_back, "input: 2"}, 1000
  end
end
