defmodule Stallion.Web.ProcessesChannelTest do
  use Stallion.Web.ChannelCase
  alias Stallion.Command.Manager

  setup do
    {:ok, event_handler} = Stallion.Command.Event.start_link
    Stallion.Command.Event.add_handler(event_handler, Stallion.Module.EventHandler, [broacaster: Stallion.Web.Endpoint])

    {:ok, manager} = Manager.start_link(event_handler)
    {:ok, %{socket: socket("id", executor: manager), manager: manager}}
  end

  test "includes running processes on connect", %{socket: socket, manager: manager} do
    {:ok, pid} = Manager.run(manager, "sleep 10")

    {:ok, _, _} = subscribe_and_join(socket, Stallion.Web.ProcessesChannel, "processes:all")
    assert_push "joined", %{processes: [_process = _]}

    Manager.kill(manager, pid)
  end

  test "it empties process list when all processes are ended", %{socket: socket, manager: manager} do
    {:ok, pid} = Manager.run(manager, "sleep 0.1")
    Manager.await(manager, pid)

    {:ok, _, _} = subscribe_and_join(socket, Stallion.Web.ProcessesChannel, "processes:all")
    assert_push "joined", %{processes: []}
  end

  test "notifies if new process is added", %{socket: socket, manager: manager} do
    {:ok, _, _} = subscribe_and_join(socket, Stallion.Web.ProcessesChannel, "processes:all")
    assert_push "joined", %{processes: []}

    {:ok, pid} = Manager.run(manager, "sleep 0.1")
    assert_push "started", %{processes: [_]}
    Manager.await(manager, pid)
    assert_push "ended", %{processes: []}
  end

end
