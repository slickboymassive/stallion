defmodule Stallion.Module.Registry.Test do
  use ExUnit.Case
  alias Stallion.Module.Registry

  setup do
    Process.register self, :test
    Registry.start_link(name: :registry)
    :ok
  end

  test "can add new modules" do
    Registry.register(:registry, :foo)
    assert Registry.get_all(:registry) == [:foo]
  end

  test "unregister modules" do
    Registry.register(:registry, :foo)
    Registry.register(:registry, :bar)
    Registry.register(:registry, :baz)
    Registry.unregister(:registry, :bar)
    assert Registry.get_all(:registry) == [:foo, :baz]
  end

end
