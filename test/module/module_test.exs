defmodule Stallion.Module.Test do
  use ExUnit.Case

  defmodule TestModule do
    use Stallion.Module

    hear ~r/foo/ do
      send :test, :got_foo
    end

    hear ~r/bar/ do
      send :test, :got_bar
      input "foo"
    end

    hear ~r/args/ do
      send :test, input
      send :test, args
      send :test, captures
    end

    exit_code 0, _ do
      send :test, 1
    end

    exit_code 1, [call_this_1: true] do
      send :test, :call_this_1
    end

    exit_code 1, call_this_2: true do
      send :test, :call_this_2
    end
  end

  setup do
    Process.register self, :test
    TestModule.start_link
    :ok
  end

  test "it creates matchers" do
    TestModule.do_hear(~r/foo/, nil, nil, nil)
    assert_receive :got_foo
  end

  test "matchers match" do
    Stallion.Matcher.match(Stallion.Matcher, "foo", [])
    assert_receive :got_foo
  end

  test "input works" do
    TestModule.input "foo"
    assert_receive :got_foo
  end

  test "args work" do
    TestModule.do_hear(~r/args/, "input", "captures", "args")
    assert_receive "input"
    assert_receive "captures"
    assert_receive "args"
  end

  test "it calls other matchers" do
    TestModule.do_hear(~r/bar/, nil, nil, nil)
    assert_receive :got_foo
    assert_receive :got_bar
  end

  test "finally matcher works" do
    TestModule.on_exit(0, "args")
    assert_receive 1
  end

  test "finally with arg matches works" do
    TestModule.on_exit(1, call_this_1: true)
    assert_receive :call_this_1

    TestModule.on_exit(1, call_this_2: true)
    assert_receive :call_this_2
  end

end
