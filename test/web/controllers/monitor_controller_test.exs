defmodule TestModule do
  use Stallion.Module

  get "/hello" do
    send_resp(conn, 200, "world")
  end

  match _ do
    send_resp(conn, 404, "oops")
  end

  hear ~r/2/ do
    send :test, {:called_back, input}
  end
end

defmodule Stallion.Web.MonitorController.Test do
  alias Stallion.Command.Manager
  use Stallion.Web.ConnCase, async: true
  alias Phoenix.Socket.Message
  import ExUnit.CaptureLog

  alias __MODULE__.Endpoint
  @port 5807

  Application.put_env(:stallion, Endpoint,
    https: false,
    http: [port: @port],
    server: true,
    debug_errors: true,
    secret_key_base: String.duplicate("abcdefgh", 8),
    pubsub: [adapter: Phoenix.PubSub.PG2, name: __MODULE__]
  )

  defmodule Endpoint do
    use Phoenix.Endpoint, otp_app: :stallion
    socket "/ws", Stallion.Web.UserSocket
  end

  setup_all do
    capture_log fn -> Endpoint.start_link() end

    {:ok, event_handler} = Stallion.Command.Event.start_link
    :ok = Stallion.Command.Event.add_handler(event_handler, Stallion.Module.EventHandler, [broacaster: Endpoint])
    {:ok, manager} = Manager.start_link(event_handler)
    Application.put_env(:stallion, Endpoint, executor: manager)
    [manager: manager]
  end

  test "lists running processes", %{manager: manager} do
    {:ok, process1} = TestModule.run("echo 1; sleep 1")
    {:ok, process2} = TestModule.run("echo 2; sleep 1")

    conn = get build_conn(), "/api/processes"
    response = json_response(conn, 200)

    assert response == [
      %{"command" => "echo 1; sleep 1", "uuid" => Manager.lookup(manager, process1).uuid, "state" => "alive"},
      %{"command" => "echo 2; sleep 1", "uuid" => Manager.lookup(manager, process2).uuid, "state" => "alive"}
    ]

    Manager.await(manager, process1)
    Manager.await(manager, process2)
  end

  test "does not show ended processes", %{manager: manager}  do
    {:ok, pid} = TestModule.run("echo 2")
    Manager.await(manager, pid)

    conn = get build_conn(), "/api/processes?state=dead"
    response = json_response(conn, 200)

    assert response == []
  end

  test "GET /" do
    conn = get build_conn(), "/"
    assert html_response(conn, 200) =~ "body"
  end

  test "GET /not_here" do
    conn = get build_conn(), "/not_here"
    assert json_response(conn, 404)["message"] == "Page not found"
  end

  test "static files" do
    conn = get build_conn(), "/static.json"
    assert json_response(conn, 200)["response"] == "ok"
  end

  test "websockets work", %{manager: manager} do
    Endpoint.start_link

    {:ok, sock} = WebsocketClient.start_link(self, "ws://127.0.0.1:#{@port}/ws/websocket")
    WebsocketClient.join(sock, "processes:all", %{})

    assert_receive %Message{
      event: "phx_reply",
      payload: %{"response" => %{}, "status" => "ok"},
      ref: "1",
      topic: "processes:all"
    }

    assert_receive %Message{event: "joined", payload: %{"processes" => []}}
    {:ok, ppid } = Manager.run(manager, "echo 2")

    assert_receive %Message{event: "started", payload: %{"processes" => [_]}, ref: nil, topic: "processes:all"}
    Manager.await(manager, ppid)

    assert_receive %Message{event: "ended", payload: %{"processes" => []}, ref: nil, topic: "processes:all"}
    WebsocketClient.close(sock)
  end
end
