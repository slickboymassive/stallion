defmodule Stallion.Command.Manager.Test do
  use ExUnit.Case, async: true
  use Retry
  alias Porcelain.Process, as: Proc
  alias Stallion.Command.Manager

  defmodule NoOpObserver do
    use GenEvent
  end

  setup do
    {:ok, gen_event_pid} = GenEvent.start_link
    GenEvent.add_handler(gen_event_pid, NoOpObserver, [])
    {:ok, manager} = Manager.start_link(gen_event_pid)
    {:ok, [manager: manager]}
  end

  test "adds running commands to registry", context do
    {:ok, pid} = Manager.run(context.manager, "echo foo")
    process = Manager.lookup(context.manager, pid)
    Proc.await(process.process)
    refute Proc.alive?(process.process)
  end

  test "show running commands in registry", context do
    {:ok, pid} = Manager.run(context.manager, "sleep 4")
    process = Manager.lookup(context.manager, pid)
    assert Proc.alive?(process.process)
  end

  test "allow to set timeout in manager", context do
    {:ok, pid} = Manager.run(context.manager, "sleep 2", timeout: 500)
    process = Manager.lookup(context.manager, pid)
    result = wait with: lin_backoff(100, 1) |> expiry(1_000) do
      Proc.alive?(process.process) == false
    end
    assert result
  end

  test "returns all currently running processes", context do
    {:ok, pid1} = Manager.run(context.manager, "sleep 4")
    {:ok, pid2} = Manager.run(context.manager, "sleep 2")

    pids = Manager.running(context.manager) |> Enum.sort |> Enum.map(fn({pid, _}) -> pid end)
    assert [^pid1, ^pid2] = pids
  end

  test "does not return ended processes when calling running", context do
    {:ok, pid} = Manager.run(context.manager, "sleep 0.1")
    Process.monitor(pid)
    Manager.await(context.manager, pid)

    assert_receive {:DOWN, _ref, :process, ^pid, :result}

    pids = Manager.running(context.manager)
    assert [] = pids
  end
end
