defmodule Stallion.Command.Runner.Test do
  use ExUnit.Case, async: true
  alias Stallion.Command

  defmodule Observer do
    use GenEvent

    def handle_event(%{type: :out, data: input, args: args}, state) do
      send :runner_test, {:stdout, input}
      send :runner_test, {:args, args}
      {:ok, state}
    end

    def handle_event(%{type: :err, data: input, args: _args}, state) do
      send :runner_test, {:stderr, input}
      {:ok, state}
    end

    def handle_event(%{type: :result, status: status, args: _args}, state) do
      send :runner_test, {:finally, status}
      {:ok, state}
    end

    def handle_event(%{type: :timeout, args: _args}, state) do
      send :runner_test, {:timeout, nil}
      {:ok, state}
    end

    def handle_event(_x, state) do
      # ignore rest
      {:ok, state}
    end
  end

  setup do
    Process.register self, :runner_test
    {:ok, handler} = Stallion.Command.Event.start_link([])
    Stallion.Command.Event.add_handler(handler, Observer, [])

    {:ok, pid} = Command.Runner.start(handler)
    {:ok, [pid: pid, handler: handler]}
  end

  test "stdout capture works", context do
    Command.Runner.run(context[:pid], "printf 'foo' > /dev/stdout", 10_000)
    assert_receive {:stdout, "foo"}
    assert_receive {:finally, _ } # wait for everything to finish
  end

  test "args works", context  do
    {:ok, runner} = Command.Runner.start(context.handler, foo: :bar)
    Command.Runner.run(runner, "printf 'foo' > /dev/stdout", 10_000)
    assert_receive {:stdout, "foo"}
    assert_receive {:args, foo: :bar}
    assert_receive {:finally, _ } # wait for everything to finish
  end

  test "finally works on success", context  do
    Command.Runner.run(context[:pid], "exit 0", 10_000)
    assert_receive {:finally, 0}
  end

  test "finally works on failure", context  do
    Command.Runner.run(context[:pid], "exit 1", 10_000)
    assert_receive {:finally, 1}
  end

  test "stderr capture works", context  do
    Command.Runner.run(context[:pid], "printf 'bar' > /dev/stderr", 10_000)
    assert_receive {:stderr, "bar"}
    assert_receive {:finally, _ } # wait for everything to finish
  end

  test "kills timeouted processes", context do
    Command.Runner.run(context[:pid], "sleep 2", 500)
    proc = Command.Runner.lookup(context[:pid])

    assert_receive {:timeout, nil}, 1000
    refute Porcelain.Process.alive?(proc.process)
  end
end
