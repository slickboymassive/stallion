defmodule Stallion.Mixfile do
  use Mix.Project

  def project do
    [app: :stallion,
     version: "0.1.0",
     elixir: "~> 1.2",
     test_coverage: [tool: ExCoveralls],
     preferred_cli_env: ["coveralls": :test, "coveralls.detail": :test, "coveralls.post": :test],
     elixirc_paths: elixirc_paths(Mix.env),
     compilers: [:phoenix] ++ Mix.compilers,
     deps: deps
   ]
  end

  # Specifies which paths to compile per environment
  defp elixirc_paths(_), do: ["test/support", "lib"]

  def application do
    [applications: [:logger, :porcelain, :cowboy, :plug, :httpoison, :poison, :phoenix_html, :phoenix, :retry],
     mod: {Stallion, []},
     env: []
    ]
  end

  defp deps do
    [
        {:porcelain, "~> 2.0"},
        {:cowboy, "~> 1.0.0"},
        {:plug, "~> 1.2"},
        {:httpoison, "~> 0.6"},
        {:poison, "~> 1.5.0"},
        {:uuid, "~> 1.1"},
        {:logger_file_backend, "~> 0.0.8"},
        {:mix_test_watch, "~> 0.2", only: :dev},
        {:phoenix, "1.2.1"},
        {:phoenix_html, "~> 2.6"},
        {:phoenix_pubsub, "~> 1.0"},
        {:websocket_client, git: "https://github.com/jeremyong/websocket_client.git", only: :test},
        {:retry, "~> 0.5.0"},
        {:excoveralls, "~> 0.5", only: :test},
        {:credo, "~> 0.4", only: [:dev, :test]}
    ]
  end
end
