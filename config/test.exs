use Mix.Config

config :stallion,
  modules: []

config :logger,
  backends: [{LoggerFileBackend, :test_log}]

config :logger, :test_log,
  path: "log/test.log",
  level: :debug,
  format: "$date $time [$level] $metadata$message\n",
  metadata: [:user_id]

# FIXME: it's stupid that we'll have to repeat this 99% identical for each application
# that uses this application. This is because render_errors etc are "compile time settings"
# so they can't be set using normal way of adding default settings (application :env in mix.exs).

config :stallion, Stallion.Web.Endpoint,
  http: [port: 6666],
  server: true,
  render_errors: [view: Stallion.Web.ErrorView, accepts: ~w(json)],
  debug_errors: false,
  pubsub: [
    adapter: Phoenix.PubSub.PG2,
    name: Stallion.Web.PubSub
  ]

