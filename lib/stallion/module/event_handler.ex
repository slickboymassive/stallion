defmodule Stallion.Module.EventHandler do
  use GenEvent
  require Logger
  alias Stallion.Matcher
  alias Stallion.Module.Registry

  def init([{:broacaster, broacaster}]) do
    {:ok, %{broadcaster: broacaster}}
  end

  def handle_event(%{type: :started, args: args}, state) do
    Logger.debug("#{__MODULE__}.handle_event/2 type: :start")
    state[:broadcaster].broadcast "processes:all", "started", %{args: args}
    {:ok, state}
  end

  def handle_event(%{type: :out, data: input, args: args}, state) do
    Logger.debug("#{__MODULE__}.handle_event/2 type: :out #{inspect(input)}")
    Matcher.match(Matcher, input, args)
    {:ok, state}
  end

  def handle_event(%{type: :err, data: input, args: args}, state) do
    Logger.debug("#{__MODULE__}.handle_event/2 type: :err #{inspect(input)}")
    Matcher.match(Matcher, input, args)
    {:ok, state}
  end

  def handle_event(%{type: :result, status: status, args: args}, state) do
    Logger.debug("#{__MODULE__}.handle_event/2 type: :result, status: #{inspect(status)}")
    Enum.each Registry.get_all(Registry), &(&1.on_exit(status, args))
    state[:broadcaster].broadcast "processes:all", "ended", %{args: args, status: status}
    {:ok, state}
  end
end
