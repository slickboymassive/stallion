defmodule Stallion.Module do
  alias Stallion.Matcher
  alias Stallion.Command.Manager

  defmacro __using__(opts) do
    base_module = __MODULE__
    module_name = opts[:name] || base_module

    quote location: :keep do
      use Plug.Router
      #use GenServer

      @regex_patterns []
      Module.register_attribute(__MODULE__, :regex_patterns, accumulate: true)

      import unquote(base_module)
      @before_compile unquote(base_module)

      @module_name unquote(module_name)

      require Logger

      plug :match
      plug :put_resp_content_type, Plug.MIME.type("json")
      plug :dispatch

      def start_link(opts \\ []) do
        add_matchers
        {:ok, _pid} = GenServer.start_link(__MODULE__, :ok, opts)
      end

      # For GenServer
      def init(:ok) do
        Logger.info "Started module #{unquote(module_name)}!"
        Process.register(self, __MODULE__)
        {:ok, []}
      end

      # For Plug
      def init(opts) do
        opts
      end

      defoverridable start_link: 1

      # TODO: convert to handle_call
      def run(command, args \\ []) do
        args = Keyword.merge([timeout: nil], args)
        Manager.run(Manager, command, args)
      end

      def input(input, args \\ []) do
        Matcher.match(Matcher, input, args)
      end

      defp do_match(conn, method, match, host) do
        send_resp(conn, 404, "oops")
      end
      defoverridable do_match: 4
    end
  end

  defmacro __before_compile__(_env) do
    quote do
      defp add_matchers do
        Enum.each(@regex_patterns, fn {pattern} ->
          Matcher.add_matcher(Matcher, pattern, fn(input, captures, args) ->
            __MODULE__.do_hear(pattern, input, captures, args)
          end)
        end)
      end
    end
  end

  defmacro exit_code(code, expected_args \\ nil, do: block) do
    quote do
      def on_exit(unquote(code), unquote(expected_args) = xargs) do
        #{ var!(args) }
        unquote(block)
      end
    end
  end

  defmacro hear(regex_pattern, do: block) do
    quote do
      @regex_patterns {unquote(regex_pattern)}
      # Using Kernel.var! to silence Credo
      def do_hear(unquote(regex_pattern), var!(input), var!(captures), var!(args)) do
        {var!(args), var!(captures), var!(input)} # Silence unused variable warnings
        unquote(block)
      end
    end
  end
end
