defmodule Stallion.Module.Registry do

  def start_link(opts) do
    name = Keyword.get(opts, :name, __MODULE__)
    Agent.start_link(fn -> [] end, name: name)
  end

  def register(registry, module) when is_atom(module) do
    Agent.update(registry, fn(modules) -> modules ++ [module] end)
  end

  def unregister(registry, module) when is_atom(module) do
    Agent.update(registry, fn(modules) -> List.delete(modules, module) end)
  end

  def get_all(registry) do
    Agent.get(registry, &(&1))
  end

end
