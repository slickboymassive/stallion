defmodule Stallion.Module.ModuleSupervisor do
  use Supervisor
  require Logger
  alias Stallion.Module.Registry

  def start_link(opts \\ []) do
    Supervisor.start_link(__MODULE__, :ok, opts)
  end

  def init(:ok) do
    modules = Application.get_env(:stallion, :modules)
    Logger.log :debug, "Starting modules! #{inspect modules}"
    case modules do
      nil -> :ignore
      [] -> :ignore
      _ ->
        children = for module <- modules, do: worker(module, [])
        Enum.each modules, &(Registry.register(Registry, &1))
        supervise(children, strategy: :one_for_one)
    end
  end
end
