defmodule Stallion.Matcher do
  use GenServer
  require Logger

  def start_link(opts) do
    Logger.debug "Starting #{opts[:name]}"
    GenServer.start_link(__MODULE__, [], opts)
  end

  def add_matcher(pid, regex, callback) do
    GenServer.cast(pid, {:add_matcher, regex, callback})
  end

  def match(pid, input, args \\ []) do
    GenServer.cast(pid, {:match, input, args})
  end

  # Callbacks

  def init(_arg) do
    {:ok, HashDict.new}
  end

  def handle_cast({:add_matcher, regex, callback}, state) do
    {:noreply, HashDict.put(state, regex, callback)}
  end

  def handle_cast({:match, input, args}, state) do
    Logger.debug("#{__MODULE__}.handle_cast/2 #{inspect(input)}")
    Enum.each(state, fn({regex, callback}) ->
      run_callback_if_match(regex, input, args, callback)
    end)
    {:noreply, state}
  end

  defp run_callback_if_match(regex, input, args, callback) do
    escaped = Regex.replace(~r/\n/, input, ~S(\\n))
    if Regex.match?(regex, input) do
      Logger.debug("#{escaped} matched #{Regex.source(regex)}, Running callback")
      captures = Regex.named_captures(regex, input)
      callback.(String.strip(input), captures, args)
    else
      Logger.debug("#{escaped} did not match #{Regex.source(regex)}")
    end
  end

end
