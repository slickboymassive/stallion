defmodule Stallion.Command.Event do
  require Logger
  @name __MODULE__

  def start_link(args \\ []) do
    GenEvent.start_link(args)
  end

  def add_handler(pid, handler, args) do
    :ok = GenEvent.add_handler(pid, handler, args)
  end

  def started(pid, args) do
    GenEvent.notify(pid, %{type: :started, args: args})
  end

  def stream_notify(pid, notification_type, payload, args) do
    GenEvent.notify(pid, %{type: notification_type, data: payload, args: args})
  end

  def finished(pid, status, args) do
    GenEvent.notify(pid, %{type: :result, status: status, args: args})
  end

  def timeout(pid, args) do
    GenEvent.notify(pid, %{type: :timeout, args: args})
  end
end
