defmodule Stallion.Command.Manager do
  alias Stallion.Command.Runner

  defmodule State do
    defstruct event_handler: nil, running: [], dead: []
  end

  use GenServer
  require Logger

  @name __MODULE__

  ## Client API
  def start_link(event_handler, opts \\ []) do
    Logger.debug "Starting #{__MODULE__}"
    state = %State{event_handler: event_handler}
    GenServer.start_link(@name, state, opts)
  end

  def run(server, command, args \\ []) do
    pid = GenServer.call(server, {:run, command, args})
    {:ok, pid}
  end

  def lookup(server, pid) do
    GenServer.call(server, {:lookup, pid})
  end

  def running(server) do
    GenServer.call(server, {:running})
  end

  def dead(server) do
    GenServer.call(server, {:dead})
  end

  def all(server) do
    running(server) ++ dead(server)
  end

  def purge(server) do
    GenServer.cast(server, {:purge})
  end

  def kill(server, pid) do
    GenServer.cast(server, {:kill, pid})
  end

  def await(server, pid) do
    GenServer.call(server, {:await, pid})
  end

  ## Server Callbacks

  def handle_call({:run, command, args}, _from, state) do
    timeout = args[:timeout] || 10_000
    {:ok, runner_pid} = Runner.start(state.event_handler, args[:args])
    Runner.run(runner_pid, command, timeout)
    Process.monitor(runner_pid)

    new_state = %State{state | running: state.running ++ [runner_pid]}
    {:reply, runner_pid, new_state}
  end

  def handle_call({:lookup, pid}, _from, state) do
    {:reply, Runner.lookup(pid), state}
  end

  def handle_call({:await, pid}, _from, state) do
    case Process.alive?(pid) do
      true ->
        process = Runner.lookup(pid)
        Porcelain.Process.await(process.process)
        {:reply, process, state}
      _ ->
        {:reply, :dead, state}
    end
  end

  def handle_call({:running}, _from, state) do
    {:reply, Enum.map(state.running, fn(pid) -> {pid, Runner.lookup(pid)} end), state}
  end

  def handle_call({:dead}, _from, state) do
    {:reply, Enum.map(state.dead, fn(pid) -> {pid, 1} end), state}
  end

  def handle_cast({:purge}, state) do
    new_state = %State{state | running: [], dead: []}
    {:noreply, new_state}
  end

  def handle_cast({:kill, pid}, state) do
    case Process.alive?(pid) do
      true ->
        process = Runner.lookup(pid)
        Porcelain.Process.stop(pid)
        {:noreply, state}
      _ ->
        {:noreply, state}
    end
  end

  def handle_cast(_, state) do
    {:noreply, state}
  end

  def handle_info({:DOWN, _ref, :process, pid, reason}, state = %State{running: pids}) when reason in [:result, :timeout] do
    new_state = %State{state | running: List.delete(pids, pid), dead: (state.dead ++ [pid])}
    {:noreply, new_state}
  end
end
