defmodule Stallion.Command.Runner do
  require Logger
  use GenServer

  def start(gen_event_pid, args_for_callbacks \\ []) do
    GenServer.start(__MODULE__, {gen_event_pid, args_for_callbacks})
  end

  def run(pid, command, timeout) do
    GenServer.cast(pid, {:run, command, timeout})
  end

  def lookup(pid) do
    GenServer.call(pid, {:lookup})
  end

  # Callbacks

  def init({gen_event_pid, args_for_callbacks}) do
    default = %{
      gen_event_pid: gen_event_pid,
      timer_pid: nil,
      process: nil,
      command: nil,
      args_for_callbacks: args_for_callbacks,
      uuid: UUID.uuid4()
    }
    {:ok, default}
  end

  def handle_call({:lookup}, _from, state) do
    {:reply, state, state}
  end

  def handle_cast({:run, command, timeout}, state) do
    opts = [
      out: {:send, self},
      err: {:send, self}
    ]

    process = Porcelain.spawn_shell(command, opts)
    timer_ref = :erlang.send_after(timeout, self(), {:timeout, process, timeout})
    new_state = %{state | timer_pid: timer_ref, process: process, command: command}

    Stallion.Command.Event.started(state.gen_event_pid, state.args_for_callbacks)
    {:noreply, new_state}
  end

  def handle_info({_pid, :data, :out, data}, state) do
    Logger.debug("#{__MODULE__}.handle_info/2 {:data, :out, #{inspect(data)}}")
    notify(state, data, :out)
    {:noreply, state}
  end

  def handle_info({_pid, :data, :err, data}, state) do
    Logger.debug("#{__MODULE__}.handle_info/2 {:data, :err, #{inspect(data)}}")
    notify(state, data, :err)
    {:noreply, state}
  end

  def handle_info({_pid, :result, %Porcelain.Result{status: status}}, state) do
    Logger.debug("#{__MODULE__}.handle_info/2 {:result, #{status}}")
    :erlang.cancel_timer(state[:timer_pid])
    Stallion.Command.Event.finished(state.gen_event_pid, status, state.args_for_callbacks)
    {:stop, :result, state}
  end

  def handle_info({:timeout, process, timeout}, state) do
    Logger.debug("#{__MODULE__}.handle_info/2 {:timeout, #{timeout}}")
    Porcelain.Process.stop(process) # implicitly makes the call to handle_info :result
    Stallion.Command.Event.timeout(state.gen_event_pid, state.args_for_callbacks)
    {:stop, :timeout, state}
  end

  defp notify(state, data, type) do
    Enum.each(normalize_input(data), fn(normalized_data) ->
      Stallion.Command.Event.stream_notify(state.gen_event_pid, type, normalized_data, state.args_for_callbacks)
    end)
  end

  defp normalize_input(data) do
    String.split(data, "\n")
  end
end
