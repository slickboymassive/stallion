defmodule Stallion.ProcessSerializer do
  def serialize(processes) do
    processes |>
      Enum.map(fn({_pid, proc}) ->
        %{
          command: proc.command,
          uuid: proc.uuid,
          state: (if Porcelain.Process.alive?(proc.process), do: :alive, else: :dead)
        }
      end)
  end
end
