defmodule Stallion.Web.Router do
  use Phoenix.Router

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :browser do
    plug :accepts, ["html"]
  end

  scope "/", Stallion.Web do
    pipe_through :browser # Use the default browser stack
    get "/", PageController, :index
  end

  scope "/api", Stallion.Web do
    pipe_through :api # Use the default browser stack
    get "/processes", MonitorController, :index
  end
end
