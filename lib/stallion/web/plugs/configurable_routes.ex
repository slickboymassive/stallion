defmodule Stallion.Web.Plug.ConfigurableRoutes do
  @doc false
  def init(default), do: default

  @doc false
  def call(conn, _opts) do
    case get_route(conn) do
      route when not is_nil(route) ->
        %{conn | path_info: Enum.drop(conn.path_info, 1)}
          |> route.call(route.init({}))
          |> Plug.Conn.halt
      _ -> conn
    end
  end

  defp get_route(conn) do
    routes = Application.get_env(:stallion, :routes, %{})

    case conn.path_info do
      [prefix | _rest] -> Map.get(routes, prefix)
      [] -> nil
    end
  end
end
