defmodule Stallion.Web.PageView do
  use Phoenix.View, root: "lib/stallion/web/templates"
  use Phoenix.HTML

  # import Stallion.Web.Router.Helpers
  # Import convenience functions from controllers
  import Phoenix.Controller, only: [
    # get_csrf_token: 0,
    # get_flash: 2,
    # view_module: 1,
    action_name: 1,
    controller_module: 1
  ]

  def handler_info(conn) do
    IO.puts "Request Handled By: #{controller_module conn}.#{action_name conn}"
  end

end
