defmodule Stallion.Web.Endpoint do
  use Phoenix.Endpoint, otp_app: :stallion
  socket "/ws", Stallion.Web.UserSocket

  plug Plug.Logger, log: :debug

  plug Plug.Static,
    at: "/", from: :stallion, gzip: false

  plug Plug.RequestId
  plug Plug.Logger

  plug Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    pass: ["*/*"],
    json_decoder: Poison

  plug Plug.MethodOverride
  plug Plug.Head

  plug Plug.Session,
    store: :cookie,
    key: "_stallion_key",
    signing_salt: "döipfw4e93e-koåphilue3h9ud"

  plug Stallion.Web.Plug.ConfigurableRoutes
  plug Stallion.Web.Router
end
