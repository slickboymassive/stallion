defmodule Stallion.Web.ProcessesChannel do
  alias Stallion.Command.Manager
  alias Stallion.ProcessSerializer
  use Phoenix.Channel

  def join("processes:all", _message, socket) do
    send(self, "joined")
    {:ok, socket}
  end

  def handle_info("joined", socket) do
    push socket, "joined", %{processes: running_processes(socket)}
    {:noreply, socket}
  end

  intercept ["started", "ended"]

  def handle_out("started", _, socket) do
    push socket, "started", %{processes: running_processes(socket)}
    {:noreply, socket}
  end

  def handle_out("ended", _, socket) do
    push socket, "ended", %{processes: running_processes(socket)}
    {:noreply, socket}
  end

  defp running_processes(socket) do
    socket.assigns.executor
      |> Manager.running
      |> ProcessSerializer.serialize
  end
end
