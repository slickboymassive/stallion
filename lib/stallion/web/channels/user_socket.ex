defmodule Stallion.Web.UserSocket do
  use Phoenix.Socket

  channel "processes*", Stallion.Web.ProcessesChannel

  transport :websocket, Phoenix.Transports.WebSocket

  def connect(_params, socket) do
    endpoint_settings = Application.get_env(:stallion, socket.endpoint)

    unless Dict.has_key?(endpoint_settings, :executor) do
      raise "An executor must be defined for application"
    end
    
    [executor: executor] = endpoint_settings
    {:ok, assign(socket, :executor, executor)}
  end

  def id(_socket), do: nil
end
