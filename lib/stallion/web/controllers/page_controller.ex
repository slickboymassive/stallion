defmodule Stallion.Web.PageController do
  use Phoenix.Controller

  def index(conn, _params) do
    conn = put_layout conn, false
    render conn, :index
  end
end
