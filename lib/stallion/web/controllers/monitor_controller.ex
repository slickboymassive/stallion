defmodule Stallion.Web.MonitorController do
  use Phoenix.Controller
  alias Stallion.Command.Manager
  alias Stallion.ProcessSerializer

  def index(conn) do
    json conn, processes
  end

  def index(conn, _) do
    json conn, processes
  end

  defp processes do
    Manager
      |> Manager.running
      |> ProcessSerializer.serialize
  end
end
