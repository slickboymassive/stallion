defmodule Stallion do
  use Application

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    require Logger
    Logger.debug "Starting Stallion"

    import Supervisor.Spec, warn: false

    children = [
      worker(Stallion.Matcher, [[name: Stallion.Matcher]]),
      worker(Stallion.Command.Event, [[name: Stallion.Command.Event]]),
      worker(Stallion.Module.Registry, [[name: Stallion.Module.Registry]]),
      worker(Stallion.Command.Manager, [Stallion.Command.Event, [name: Stallion.Command.Manager]]),
      supervisor(Stallion.Module.ModuleSupervisor, [[name: Stallion.Module.ModuleSupervisor]]),
      supervisor(Stallion.Web.Endpoint, [])
    ]

    Logger.debug "Starting Stallion supervisors!"
    opts = [strategy: :one_for_one, name: Sup.Supervisor]
    {:ok, pid} = Supervisor.start_link(children, opts)

    Stallion.Command.Event.add_handler(Stallion.Command.Event, Stallion.Module.EventHandler, [broacaster: Stallion.Web.Endpoint])

    {:ok, pid}
  end
end
